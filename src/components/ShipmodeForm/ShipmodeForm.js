import React, {Component} from 'react';
import './ShipmodeForm.css';
import PropTypes from 'prop-types';
import shipmodeShape from '../../shapes/shapes';

class ShipmodeForm extends Component {
    constructor(props) {
        super(props);
        this.state = {item: this.props.item};

        this.validateString = this.validateString.bind(this);
        this.validateNumber = this.validateNumber.bind(this);
        this.updateItem = this.updateItem.bind(this);
    }

    componentWillReceiveProps(props) {
        this.setState({item: props.item});
    }

    validateString(event) {
        const key = event.target.id;
        const value = event.target.value;
        this.updateItem(key, value);
    }

    validateNumber(event) {
        const key = event.target.id;
        const value = event.target.value;
        if (!isNaN(value) && value.length > 0) {
            this.updateItem(key, parseFloat(value));
        }
    }

    updateItem(key, value) {
        const item = this.state.item;
        item[key] = value;
        const context = this;
        this.setState({item: item}, function() {
            context.props.updateItem(item);
        });
    }

    render() {
        let form = null;
        if (this.state.item == null) {
            form =
                <form className="shipmode-form">
                    <ul><li><label>No data</label></li></ul>
                </form>;
        } else {
            form =
                <form className="shipmode-form">
                    <ul className="flex-outer">
                        <li>
                            <label>Shipmode Carrier</label>
                            <input type="text" id="shipmode-carrier" value={this.state.item['shipmode-carrier']} onChange={this.validateString} />
                        </li>
                        <li>
                            <label>Shipmode Code</label>
                            <input type="text" id="shipmode-code" value={this.state.item['shipmode-code']} onChange={this.validateString} />
                        </li>
                        <li>
                            <label>Shipcode</label>
                            <input type="text" id="calcode-shipcode" value={this.state.item['calcode-shipcode']} onChange={this.validateString} />
                        </li>
                        <li>
                            <label>Display Name</label>
                            <input type="text" id="calcode-desc-displayName" value={this.state.item['calcode-desc-displayName']} onChange={this.validateString} />
                        </li>
                        <li>
                            <label>Display Text</label>
                            <input type="text" id="calcodedes-displayText" value={this.state.item['calcodedes-displayText']} onChange={this.validateString} />
                        </li>
                        <li>
                            <label>Return Text</label>
                            <input type="text" id="calcodedes-long-returnText" value={this.state.item['calcodedes-long-returnText']} onChange={this.validateString} />
                        </li>
                        <li>
                            <label>Field 1</label>
                            <input type="text" id="calcode-field1" value={this.state.item['calcode-field1']} onChange={this.validateString} />
                        </li>
                        <li>
                            <label>Shipping Cost</label>
                            <input type="text" id="calrlookup-shipcharge-value" value={this.state.item['calrlookup-shipcharge-value']} onChange={this.validateNumber} />
                        </li>
                        <li>
                            <label>Basket Order</label>
                            <input type="text" id="calrule-field1-basketOrder" value={this.state.item['calrule-field1-basketOrder']} onChange={this.validateNumber} />
                        </li>
                        <li>
                            <label>Non Promise Dates</label>
                            <input type="text" id="calrule-field2-nonpromisedate" value={this.state.item['calrule-field2-nonpromisedate']} onChange={this.validateString} />
                        </li>
                    </ul>
                </form>;
        }
        return (
            <div className="form-container-container">
                <div className="form-container">
                    {form}
                </div>
            </div>
        );
    }
}

// noinspection JSUnresolvedVariable
ShipmodeForm.propTypes = {
    item: shipmodeShape.isRequired,
    updateItem: PropTypes.func.isRequired,
};

export default ShipmodeForm;
